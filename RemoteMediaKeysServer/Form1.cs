﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteMediaKeysServer
{
    public partial class Form1 : Form
    {

        public static object th;
        public static bool started = false;

        UDPListener listener;

        public Form1()
        {
            InitializeComponent();

            th = this;
            listener = new UDPListener();
        }


        public void setStatus(string st)
        {
            statusbox.Text =  st + "\n" + statusbox.Text;
        }

        public void Start()
        {
            if (!started)
            {
                setStatus("------------- START -------------");
                listener.StartListener(Convert.ToInt32(portno.Text));

                listener.NewMessageReceived += this.OnMessage;
                started = true;
            } else
            {
                setStatus("------------- RUNNING -------------");
            }
        }

        public void Stop()
        {
            listener.NewMessageReceived -= this.OnMessage;
            listener.StopListener();
            started = false;
            setStatus("------------- STOP -------------");
        }

        private void start_Click(object sender, EventArgs e)
        {
            Start();

        }

        private void stop_Click(object sender, EventArgs e)
        {
            Stop();
        }
        

        public void OnMessage(object sender, MyMessageArgs args)
        {
            

            switch (Convert.ToInt32(args.data))
            {

                case InKey.PLAY_PAUSE:

                     KeyBoard.Press(KeyBoard.Key.VK_MEDIA_PLAY_PAUSE);
                   
                    break;

                case InKey.NEXT:
                    
                     KeyBoard.Press(KeyBoard.Key.VK_MEDIA_NEXT_TRACK);
                   
                    break;
                case InKey.PREV:

                    KeyBoard.Press(KeyBoard.Key.VK_MEDIA_PREV_TRACK);

                    break;
                case InKey.STOP:

                    KeyBoard.Press(KeyBoard.Key.VK_MEDIA_STOP);

                    break;
                case InKey.MUTE:

                    KeyBoard.Press(KeyBoard.Key.VK_VOLUME_MUTE);

                    break;
                case InKey.DOWN:

                    KeyBoard.Press(KeyBoard.Key.VK_VOLUME_DOWN);

                    break;
                case InKey.UP:

                    KeyBoard.Press(KeyBoard.Key.VK_VOLUME_UP);

                    break;
                    

            }

            setStatus("Received : " + args.data);
            
        }





        private void button1_Click(object sender, EventArgs e)
        {
            
                KeyBoard.Press(KeyBoard.Key.VK_MEDIA_NEXT_TRACK);
           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Start();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Stop();
        }
    }
}

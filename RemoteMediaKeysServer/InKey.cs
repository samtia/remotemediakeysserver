﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RemoteMediaKeysServer
{
    class InKey
    {
        public const int PLAY_PAUSE = 1;

        public const int NEXT = 2;
        public const int PREV = 3;

        public const int STOP = 4;

        public const int MUTE = 5;
        public const int UP = 6;
        public const int DOWN = 7;



    }

}

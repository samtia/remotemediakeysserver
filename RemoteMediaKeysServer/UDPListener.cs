﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RemoteMediaKeysServer
{
    class UDPListener
    {
        private int m_portToListen = 11000;
        private volatile bool listening;
        volatile Thread m_ListeningThread;
        public event EventHandler<MyMessageArgs> NewMessageReceived;

        UdpClient listener;

        //constructor
        public UDPListener()
        {
            this.listening = false;
        }

        public void StartListener(int portno)
        {
            if (!this.listening)
            {
                this.m_portToListen = portno;
                m_ListeningThread = new Thread(ListenForUDPPackages);
                m_ListeningThread.IsBackground = true;
                this.listening = true;
                m_ListeningThread.Start();
            }
        }

        public void StopListener()
        {
            this.listening = false;
            listener.Close();
           
           
        }



        protected virtual void OnNewMessageReceived(MyMessageArgs e)
        {
            EventHandler<MyMessageArgs> handler = NewMessageReceived;
            if (handler != null)
            {
                handler(this, e);
            }
        }


        public void ListenForUDPPackages()
        {
            listener = null;
            try
            {
                listener = new UdpClient(m_portToListen);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            if (listener != null)
            {
                IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, m_portToListen);

                try
                {
                    while (this.listening)
                    {
                         byte[] bytes = listener.Receive(ref groupEP);

                        string nd = Encoding.ASCII.GetString(bytes, 0, bytes.Length);

                        NewMessageReceived.Raise(this, new MyMessageArgs(nd));

                        //raise event                        
                        //OnNewMessageReceived(new MyMessageArgs(nd));
                    }
                }
                catch (Exception e)
                {
                  //  MessageBox.Show(e.ToString());
                }
                finally
                {
               //     listener.Close();
                    // Console.WriteLine("Done listening for UDP broadcast");
                }
            }
        }
    }

    public class MyMessageArgs : EventArgs
    {
        public string data { get; set; }

        public MyMessageArgs(string newData)
        {
            data = newData;
        }
    }




    /// <summary>Extension methods for EventHandler-type delegates.</summary>
    public static class EventExtensions
    {
        /// <summary>Raises the event (on the UI thread if available).</summary>
        /// <param name="multicastDelegate">The event to raise.</param>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">An EventArgs that contains the event data.</param>
        /// <returns>The return value of the event invocation or null if none.</returns>
        public static object Raise(this MulticastDelegate multicastDelegate, object sender, MyMessageArgs e)
        {
            object retVal = null;

            MulticastDelegate threadSafeMulticastDelegate = multicastDelegate;
            if (threadSafeMulticastDelegate != null)
            {
                foreach (Delegate d in threadSafeMulticastDelegate.GetInvocationList())
                {
                    var synchronizeInvoke = d.Target as ISynchronizeInvoke;
                    if ((synchronizeInvoke != null) && synchronizeInvoke.InvokeRequired)
                    {
                        retVal = synchronizeInvoke.EndInvoke(synchronizeInvoke.BeginInvoke(d, new[] { sender, e }));
                    }
                    else
                    {
                        retVal = d.DynamicInvoke(new[] { sender, e });
                    }
                }
            }

            return retVal;
        }
    }
}

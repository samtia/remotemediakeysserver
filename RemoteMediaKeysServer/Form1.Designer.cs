﻿namespace RemoteMediaKeysServer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.start = new System.Windows.Forms.Button();
            this.stop = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.statusbox = new System.Windows.Forms.RichTextBox();
            this.portno = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // start
            // 
            this.start.Location = new System.Drawing.Point(12, 53);
            this.start.Name = "start";
            this.start.Size = new System.Drawing.Size(118, 39);
            this.start.TabIndex = 0;
            this.start.Text = "Start Server";
            this.start.UseVisualStyleBackColor = true;
            this.start.Click += new System.EventHandler(this.start_Click);
            // 
            // stop
            // 
            this.stop.Location = new System.Drawing.Point(150, 53);
            this.stop.Name = "stop";
            this.stop.Size = new System.Drawing.Size(118, 39);
            this.stop.TabIndex = 1;
            this.stop.Text = "Stop Server";
            this.stop.UseVisualStyleBackColor = true;
            this.stop.Click += new System.EventHandler(this.stop_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(77, 215);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(119, 34);
            this.button1.TabIndex = 3;
            this.button1.Text = "Test Key";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // statusbox
            // 
            this.statusbox.Location = new System.Drawing.Point(12, 98);
            this.statusbox.Name = "statusbox";
            this.statusbox.Size = new System.Drawing.Size(256, 111);
            this.statusbox.TabIndex = 4;
            this.statusbox.Text = "";
            // 
            // portno
            // 
            this.portno.Location = new System.Drawing.Point(77, 27);
            this.portno.Name = "portno";
            this.portno.Size = new System.Drawing.Size(118, 20);
            this.portno.TabIndex = 5;
            this.portno.Text = "11000";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(280, 261);
            this.Controls.Add(this.portno);
            this.Controls.Add(this.statusbox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.stop);
            this.Controls.Add(this.start);
            this.Name = "Form1";
            this.Text = "Remote Media Keys Server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button start;
        private System.Windows.Forms.Button stop;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox statusbox;
        private System.Windows.Forms.TextBox portno;
    }
}


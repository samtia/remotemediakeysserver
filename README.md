# Remote Media Keys - Server - Windows #

Controlling Media Keys (For running Media Player) remotely (Preferably Android (Android Repo Coming Soon)) in Local Network Over UDP 

### DONE TASKS ###

* UDP SERVER LISTENING
* CROSS THREAD Communication
* UI Design (Basic) SERVER
* KeyBoard KeyPress Raise Event
* KEYS assigned to UDP Commands

### TODO ###

* STABALIZE SERVER
* ANDROID APP

### ISSUE ###

* KeyBoard KeyPress Event is Working good when raised via ButtonClick but not working when UDP event is triggered. Same Funcction , Same Thread m No Exception , No Error. It just does not Work

https://bitbucket.org/samtia/remotemediakeysserver/issues/1/keyboard-keypress-event-dosnt-trigger-on

### Wanna Participate? ###

* Fork the Repo or Generate a Pull Request
* Want any Other Feature ? Mail me at me@atiqsamtia.com